# seetaface6JNI

交流QQ群：891670629（已满）、694350800

注意：jdk版本要1.8.0_261以上

#### 项目介绍
基于中科院seetaface6进行封装的JAVA人脸识别算法库，支持人脸检测、人脸提取、1:1比对、1:N比对、静默图片活体检测。
seetaface6：https://github.com/seetafaceengine/SeetaFace6

#### 环境配置
1、下载model（ 链接：https://pan.baidu.com/s/1FYz4muY3X4tOKIKhb5kBVA , 提取码：b6o4 ） 文件到本地，并解压出来；

2、下载build/lib目录中对应的库文件到本地：Windows(64位)环境下载win-x64；

3、将build中的faces-data.db下载到本地；（PS：如果不需要使用1:N人脸搜索,不需要此文件，需要将seetafce.properties中的sqlite.db.file配置注释掉）；

4、将src/main/resources/中的seetaface.properties文件放到项目的resources根目录中；

```properties
#依赖的lib名称，注意依赖关系顺序，用逗号隔开
#windows os
libs=tennis,tennis_haswell,tennis_pentium,tennis_sandy_bridge,SeetaAuthorize,SeetaFaceAntiSpoofingX600,\
  SeetaFaceDetector600,SeetaFaceLandmarker600,SeetaFaceRecognizer610,SeetaFace6JNI

#依赖的lib存放目录, 等同于-Djava.library.path=
#windows os
libs.path=E:\\Projects\\java\\cnsugar\\seetaface6JNI-sample\\lib\\win-x64

#seetaface6 model目录
bindata.dir=E:/ai-face/SeetaFace6JNI/models/sf3.0_models

#sqlite config
sqlite.db.file=./faces-data.db
sqlite.conn.maxTotal=50
sqlite.conn.maxIdle=5
sqlite.conn.minIdle=0
sqlite.conn.maxWaitMillis=60000
```


5、将build目录下的seetaface6JNI-1.0.0.jar和依赖包导入到项目中，pom如下:

```xml
   <properties>
       <spring.version>4.2.8.RELEASE</spring.version>
       <log4j.version>2.8.2</log4j.version>
       <slf4j.version>1.7.25</slf4j.version>
   </properties>
  
   <dependencies>
       <dependency>
           <groupId>com.cnsugar.ai</groupId>
           <artifactId>seetaface6JNI</artifactId>
           <version>1.0.0</version>
           <scope>system</scope>
           <systemPath>${project.basedir}/lib/seetaface6JNI-1.0.0.jar</systemPath>
       </dependency>

       <dependency>
           <groupId>org.springframework</groupId>
           <artifactId>spring-core</artifactId>
           <version>${spring.version}</version>
       </dependency>
  
       <dependency>
         <groupId>org.slf4j</groupId>
         <artifactId>slf4j-api</artifactId>
         <version>${slf4j.version}</version>
       </dependency>
       <!-- log4j2 -->
       <dependency>
         <groupId>org.apache.logging.log4j</groupId>
         <artifactId>log4j-api</artifactId>
         <version>${log4j.version}</version>
       </dependency>
       <dependency>
         <groupId>org.apache.logging.log4j</groupId>
         <artifactId>log4j-core</artifactId>
         <version>${log4j.version}</version>
       </dependency>
       <!--  log4j to slf4j bridge -->
       <dependency>
         <groupId>org.apache.logging.log4j</groupId>
         <artifactId>log4j-slf4j-impl</artifactId>
         <version>${log4j.version}</version>
       </dependency>
   
       <!-- sqlite -->
       <dependency>
         <groupId>org.xerial</groupId>
         <artifactId>sqlite-jdbc</artifactId>
         <version>3.25.2</version>
       </dependency>
       <dependency>
         <groupId>org.apache.commons</groupId>
         <artifactId>commons-pool2</artifactId>
         <version>2.4.2</version>
       </dependency>
       <dependency>
         <groupId>commons-io</groupId>
         <artifactId>commons-io</artifactId>
         <version>2.6</version>
       </dependency>
   </dependencies> 
```

6、调用FaceHelper中的方法。


#### 使用方法
所有方法都封装到了FaceHelper工具类中
```java
    /**
     * 人脸比对
     *
     * @param img1
     * @param img2
     * @return 相似度 (-1：第一张图无人脸; -2：第二张图无人脸)
     */
    float compare(File img1, File img2);
    float compare(byte[] img1, byte[] img2);
    float compare(BufferedImage image1, BufferedImage image2);

    /**
     * 注册人脸(会对人脸进行裁剪)
     *
     * @param key   人脸照片唯一标识
     * @param image 人脸照片
     * @return
     */
    boolean register(String key, BufferedImage image);
    boolean register(String key, byte[] img);

    /**
     * 注册人脸
     *
     * @param image 人脸照片
     * @return
     */
    long register(BufferedImage image);
    long registerCroppedFace(byte[] croppedFace);

    /**
     * 搜索人脸
     *
     * @param img 人脸照片
     * @return
     */
    Result search(byte[] img);
    Result search(BufferedImage image);

    /**
     * 用裁剪后的人脸搜索
     * @param croppedFace
     * @return
     */
    Result searchByCroppedFace(byte[] croppedFace);

    /**
     * 人脸提取（裁剪）
     *
     * @param image
     * @return return cropped face
     */
    byte[][] cropFace(BufferedImage image);
    byte[][] cropFace(byte[] img);

    /**
     * 人脸提取（裁剪）
     *
     * @param image
     * @return return cropped face
     */
    BufferedImage[] crop(BufferedImage image);\
    BufferedImage[] crop(byte[] img);

    /**
     * 人脸识别
     *
     * @param img
     * @return
     */
    SeetaRect[] detect(byte[] img);
    SeetaRect[] detect(BufferedImage image);

    /**
     * 人脸特征识别
     *
     * @param image
     * @return
     */
    FaceLandmark[] detectLandmark(BufferedImage image);

    /**
     * 提取人脸区域特性
     *
     * @param face
     * @return
     */
    float[] extractCroppedFace(byte[] face);

    /**
     * 提取一个图像中最大人脸的特征
     *
     * @param image
     * @return
     */
   float[] extractMaxFace(BufferedImage image);

    /**
     * 计算两个特性的相似度
     *
     * @param features1
     * @param features2
     * @return
     */
    float calculateSimilarity(float[] features1, float[] features2);

    /**
     * 静默图片活体检测
     * REAL = 0,       ///< 真实人脸
     * SPOOF = 1,      ///< 攻击人脸（假人脸）
     * FUZZY = 2,      ///< 无法判断（人脸成像质量不好）
     * DETECTING = 3,  ///< 正在检测
     * @param img
     * @return
     */
    FaceAntiSpoofingStatus predictImage(byte[] img);
    FaceAntiSpoofingStatus predictImage(BufferedImage image);

    /**
     * 删除已注册的人脸
     *
     * @param keys
     */
    long removeRegister(String... keys);

    /**
     * 清除人脸库数据
     */
    void clear();
    
```

- 示例代码：1:1人脸比对
```java
    @org.junit.Test
    public void testCompare() throws Exception {
        String img1 = "F:\\ai\\demo-pic39.jpg";
        String img2 = "F:\\ai\\left_pic_one.jpg";
        System.out.println("result:"+FaceHelper.compare(new File(img1), new File(img2)));
    }
```

- 示例代码：1:N人脸搜索
  先调用FaceHelper.register()方法将人脸图片注册到seetaface6的人脸库(内存)中，同时会将图片存在sqlite数据库中进行持久化，下次应用程序启动时会自动从sqlite中把图片读取出来重新注册到seetafce6的内存库中

```java
    @org.junit.Test
    public void testRegister() throws IOException {
        //将F:\ai\star目录下的jpg、png图片都注册到人脸库中，以文件名为key
        Collection<File> files = FileUtils.listFiles(new File("F:\\ai\\star"), new String[]{"jpg", "png"}, false);
        for (File file : files) {
            String key = file.getName();
            try {
                FaceHelper.register(key, FileUtils.readFileToByteArray(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @org.junit.Test
    public void testSearch() throws IOException {
        SeetafaceBuilder.build();//系统启动时先调用初始化方法

        //等待初始化完成
        while (SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.LOADING || SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.READY) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        long l = System.currentTimeMillis();
        Result result = FaceHelper.search(FileUtils.readFileToByteArray(new File("F:\\ai\\gtl.jpg")));
        System.out.println("搜索结果：" + result + "， 耗时：" + (System.currentTimeMillis() - l));
    }
```


- 示例代码：活体检测
```java
    @org.junit.Test
    public void testPredictImage() throws IOException {
        FaceAntiSpoofingStatus status = FaceHelper.predictImage(FileUtils.readFileToByteArray(new File("D:\\face\\gtl.png")));
        System.out.println(status);
    }
```

如果对您有帮助，欢迎点Star或捐赠
