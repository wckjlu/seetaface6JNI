import com.cnsugar.ai.face.FaceHelper;
import com.cnsugar.ai.face.SeetafaceBuilder;
import com.cnsugar.ai.face.bean.Result;
import com.cnsugar.ai.face.utils.ImageUtils;
import com.seetaface.model.FaceAntiSpoofingStatus;
import com.seetaface.model.FaceLandmark;
import com.seetaface.model.SeetaPointF;
import com.seetaface.model.SeetaRect;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;

public class Test {

    private void init() {
        SeetafaceBuilder.build();//系统启动时先调用初始化方法

        //等待初始化完成
        while (SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.LOADING || SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.READY) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 检测人脸
     * @throws IOException
     */
    @org.junit.Test
    public void testDetect() throws IOException {
        SeetaRect[] rects = FaceHelper.detect(FileUtils.readFileToByteArray(new File("D:\\face\\hz2.jpg")));
        if (rects != null) {
            for (SeetaRect rect : rects) {
                System.out.println("x="+rect.x+", y="+rect.y+", width="+rect.width+", height="+rect.height+", score="+rect.score);
            }
        }
    }

    /**
     * 检测人脸并返回每个人脸在图像中的5个特性点坐标
     * @throws IOException
     */
    @org.junit.Test
    public void testDetectLandmark() throws IOException {
        FaceLandmark[] landmarks = FaceHelper.detectLandmark(ImageIO.read(new File("D:\\face\\hz2.jpg")));
        if (landmarks != null) {
            for (FaceLandmark landmark : landmarks) {
                System.out.println(landmark.toString());
            }
        }
    }

    /**
     * 提取人脸区域特性
     * @throws IOException
     */
    @org.junit.Test
    public void testExtractCroppedFace() throws IOException {
        byte[][] faces = FaceHelper.cropFace(ImageIO.read(new File("D:\\face\\hz2.jpg")));
        if (faces == null) {
            return;
        }
        for (byte[] face : faces) {
            float[] features = FaceHelper.extractCroppedFace(face);
            if (features != null) {
                System.out.println(Arrays.toString(features));
            }
        }

    }

    /**
     * 提取一个图像中最大人脸的特征
     * @throws IOException
     */
    @org.junit.Test
    public void testExtractMaxFace() throws IOException {
        float[] features = FaceHelper.extractMaxFace(ImageIO.read(new File("D:\\face\\hz2.jpg")));
        if (features != null) {
            System.out.println(Arrays.toString(features));
        }
    }

    /**
     * 计算两个特性的相似度
     * @throws IOException
     */
    @org.junit.Test
    public void testCalculateSimilarity() throws IOException {
        String img1 = "D:\\face\\gtl.png";
        String img2 = "D:\\face\\gtl1.png";
//        float[] features1 = FaceHelper.extractMaxFace(ImageIO.read(new File(img1)));
//        float[] features2 = FaceHelper.extractMaxFace(ImageIO.read(new File(img2)));

        byte[] face1 = FaceHelper.cropFace(ImageIO.read(new File(img1)))[0];
        byte[] face2 = FaceHelper.cropFace(ImageIO.read(new File(img2)))[0];
        float[] features1 = FaceHelper.extractCroppedFace(face1);
        float[] features2 = FaceHelper.extractCroppedFace(face2);

        System.out.println("features1: "+Arrays.toString(features1));
        System.out.println("features2: "+Arrays.toString(features2));
        System.out.println("result:" + FaceHelper.calculateSimilarity(features1, features2));
    }

    @org.junit.Test
    public void testCompare() throws Exception {
        String img1 = "D:\\face\\gtl.png";
        String img2 = "D:\\face\\gtl1.png";
        System.out.println("result:" + FaceHelper.compare(new File(img1), new File(img2)));
    }

    @org.junit.Test
    public void testRegisterAndQuery() throws IOException {
        testRegister();
        File file = new File("D:\\face\\gtl1.png");
        Result result = FaceHelper.search(FileUtils.readFileToByteArray(file));
        System.out.println("搜索结果：" + result);
    }

    @org.junit.Test
    public void testRegister() throws IOException {
        //将F:\ai\star目录下的jpg、png图片都注册到人脸库中，以文件名为key
        Collection<File> files = FileUtils.listFiles(new File("D:\\face"), new String[]{"jpg", "png"}, false);
        for (File file : files) {
            String key = file.getName();
            try {
                FaceHelper.register(key, FileUtils.readFileToByteArray(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println(1);
    }

    @org.junit.Test
    public void testSearch() throws IOException {
        init();

        long l = System.currentTimeMillis();
        Result result = FaceHelper.search(FileUtils.readFileToByteArray(new File("D:\\face\\fb'b.png")));
        System.out.println("搜索结果：" + result + "， 耗时：" + (System.currentTimeMillis() - l));
    }

    @org.junit.Test
    public void testCorp() throws IOException {
        File file = new File("D:\\face\\hz1.jpg");
        BufferedImage[] images = FaceHelper.crop(FileUtils.readFileToByteArray(file));
        if (images != null) {
            int i = 0;
            for (BufferedImage image : images) {
                ImageIO.write(image, "jpg", new File(file.getAbsolutePath()+"-corp-"+(i++)+".jpg"));
            }
        }
    }

    @org.junit.Test
    public void testDelete() {
        init();
        FaceHelper.removeRegister("fbb.png");
    }

    @org.junit.Test
    public void testPredictImage() throws IOException {
        FaceAntiSpoofingStatus status = FaceHelper.predictImage(FileUtils.readFileToByteArray(new File("D:\\face\\fbb.png")));
        System.out.println(status);
    }

    @org.junit.Test
    public void testJVM() throws Exception {
//        init();
//        testRegister();
        for (int i = 0; i < 100000; i++) {
//            long l = System.currentTimeMillis();
//            Result result = FaceHelper.search(FileUtils.readFileToByteArray(new File("D:\\face\\yg.png")));
//            System.out.println(i+"-搜索结果：" + result + "， 耗时：" + (System.currentTimeMillis() - l));
            testCompare();
        }
    }
}
