package com.seetaface.model;

public class SeetaPointF {
    public double x;
    public double y;

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
